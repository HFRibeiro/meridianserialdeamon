from flask import Flask, request

import threading 
import ctypes 
import time 

import serial
import time

import signal
import sys, os

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    xonxoff=True
)

app = Flask(__name__)

@app.route('/')
def hello():
    data = request.args.get('data', default = 'empty', type = str)
    print(data)
    try:
        ser.flushInput() #flush input buffer, discarding all its contents
        ser.flushOutput()#flush output buffer, aborting current output 
                    #and discard all that is in buffer

        #write data
        ser.write(str(data+"\n").encode())
        print("write data: "+data)

    except:
        print ("error on write")

    return "Hello World!"+data
   
class thread_with_exception(threading.Thread): 
    def __init__(self, name): 
        threading.Thread.__init__(self) 
        self.name = name 
              
    def run(self): 
  
        # target function of the thread class 
        try: 
            while True: 
                try: 
                    reading = ser.readline().decode()
                    #print("Serial read: "+reading)
                except: 
                    print ("error on red")
        finally: 
            print('ended') 
           
    def get_id(self): 
  
        # returns id of the respective thread 
        if hasattr(self, '_thread_id'): 
            return self._thread_id 
        for id, thread in threading._active.items(): 
            if thread is self: 
                return id
   
    def raise_exception(self): 
        thread_id = self.get_id() 
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
              ctypes.py_object(SystemExit)) 
        if res > 1: 
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
            print('Exception raise failure') 
       
t1 = thread_with_exception('Thread 1') 
t1.start() 

def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    os._exit(1)

signal.signal(signal.SIGINT, signal_handler)


if __name__ == '__main__':
    app.run(host= '0.0.0.0', port=80)